import discord
from discord.ext import commands

class Game(commands.Cog):
    def __init__(self, bot) -> None:
        super().__init__()
        self.client = bot

def setup(client):
	client.add_cog(Game(client))