import discord
import asyncio
import yaml
import random
from yaml import YAMLError
from discord.ext import commands

class StartGame(commands.Cog):
    def __init__(self, bot) -> None:
        super().__init__()
        self.client = bot
        self.players = []

    def professions(self) -> None:
        professions = open("prof.yaml", "r")
        professions = yaml.safe_load(professions)

        dark_players = random.sample(self.players, random.randint(1, len(self.players)//2 + 1))
        dark_players = {i:random.choice(professions["dark"]) for i in dark_players}

        red_players = [i for i in self.players if i not in dark_players]
        red_players = {i:random.choice(professions["red"]) for i in red_players}

        self.players = {"dark": dark_players, "red": red_players} 

        with open("players.yaml", "w") as open_file:
            try:
                yaml.dump(self.players, open_file)
            except YAMLError:
                print('Иди нахер кароч')

    @commands.command(aliases=['mafia'])
    async def start_game(self, ctx: commands.Context)-> None:

        embed=discord.Embed(title="Mafia", color=0xff8800)
        embed.add_field(name="Прием участников на игру в Мафию", value="Требуется 8 или более игроков", inline=False)
        embed.add_field(name="Для вступления в игру:", value="Поставьте :leaves: для соглашения в игре", inline=False)
        embed.set_footer(text="Made by Corvus Corax :D")

        msg = await ctx.send(embed=embed)

        await msg.add_reaction('🍃')

        await asyncio.sleep(5)

        msg = discord.utils.get(self.client.cached_messages, id=msg.id)

        for reaction in msg.reactions:
            if reaction.emoji == '🍃':
                async for user in reaction.users():
                    if user.id != msg.author.id:
                        self.players.append(user.id)
                break

        if 1 <= len(self.players) <= 3:
            self.professions()
        self.players.clear()


def setup(client):
	client.add_cog(StartGame(client))