import discord
import yaml
from discord.ext import commands

class Info(commands.Cog):
    def __init__(self, bot) -> None:
        super().__init__()
        self.client = bot

    @commands.command()
    async def help(self, ctx: commands.Context) -> str:
        embed=discord.Embed(title="Мафия", description="В мирном городе появляется мафия, и честные жители больше не могут спать спокойно: им нужно вычислить кто есть кто и выгнать всю мафию, чтобы спастись. Если им не удастся это сделать, мафия захватит город, и мирные жители сдохнут. 0_0", color=0x850054)
        embed.add_field(name="Правила:", value=":D", inline=False)
        embed.add_field(name="1.", value="Игра делится на два периода: день и ночь. ", inline=True)
        embed.add_field(name="2.", value="Ночью все игроки засыпают и по команде ведущего просыпаются отдельные игроки и выполняют свою роль.", inline=True)
        embed.add_field(name="3.", value="Днем просыпаются все игроки и пытаются распознать мафию или других отрицательных персонажей.", inline=True)
        embed.add_field(name="4.", value="После дневного обсуждения наступает вечер, когда каждый игрок имеет в своем распоряжении одну минуту, чтобы высказаться.", inline=True)
        embed.add_field(name="5.", value="После того, как все игроки высказались, наступает голосование за кик игрока.", inline=True)
        embed.add_field(name="6.", value="У кикнутого игрока будет ещё полминуты на свои последние слова.", inline=True)
        embed.add_field(name="7.", value="Человек, которого выгнали не обязан говорить свою роль.", inline=True)
        embed.add_field(name="8.", value="Красные игроки - положительные роли. Черные игроки - отрицательные ", inline=True)
        embed.add_field(name="9.", value="Любой игрок, даже мафия может расскрыться доктором, шерифом и т.д.", inline=True)
        await ctx.send(embed=embed)

    @commands.command()
    async def prof(self, ctx: commands.Context) -> str:
        embed=discord.Embed(title="Профессии", description="Подробное описание профессий в игре \"Мафиа\"")
        
        with open("prof.yaml") as open_file:

            data = yaml.safe_load(open_file)

            for prof in data["white"]:
                if data["white"][prof]['aliases']:
                    embed.add_field(name=' / '.join(map(str,data["white"][prof]["aliases"])), value=data["white"][prof]["description"], inline=False)
                else:
                    embed.add_field(name=prof, value=data["white"][prof]["description"], inline=False)

            for prof in data["dark"]:
                if data["dark"][prof]["aliases"]:
                    embed.add_field(name=' / '.join(map(str,data["dark"][prof]["aliases"])), value=data["dark"][prof]["description"], inline=False)
                else:
                    embed.add_field(name=prof, value=data["dark"][prof]["description"], inline=False)

        embed.set_footer(text="С описанием помог Славик :D")
        await ctx.send(embed=embed)


def setup(client):
	client.add_cog(Info(client))