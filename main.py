import os
import discord
from settings import TOKEN
from discord.ext import commands

class Client(commands.Bot):
    def __init__(self):
        super().__init__(command_prefix=commands.when_mentioned_or(">>"),intents=discord.Intents.all())

    async def on_ready(self):
        print(f"Logged in as {self.user} (ID: {self.user.id})")
        print("------")
        print("Discord Mafia\nMade Raven aka Corvus Corax aka Posel ti nahui ya tut ne budu perechislat vse niki")


client = Client()
client.remove_command('help')

for filename in os.listdir("./cogs"):
    if filename.endswith('.py'):
        client.load_extension(f"cogs.{filename[:-3]}")

client.run(TOKEN)